﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RepasoProgra
{
    class Lista
    {
        static void Main()
        {
            List<string> nombres = new List<string>();

            nombres.Add("Vegeta");
            nombres.Add("Willyrex");
            nombres.Add("Pepe");
            nombres.Add("Ana");
            nombres.Add("Pablo");

            Console.WriteLine("Nombres en lista:");
            Console.WriteLine("");
            foreach (string nombre in nombres)
            {
                Console.WriteLine(nombre);
            }

            // el nimbre esta en la lista?
            Console.WriteLine("");
            Console.Write("Busque un nombre: ");
            string nombreBuscado = Console.ReadLine();

            if (nombres.Contains(nombreBuscado))
            {
                Console.WriteLine(nombreBuscado + " está en la lista de invitados");
            }
            else
            {
                Console.WriteLine(nombreBuscado + " no está en la lista de invitados");
            }
        }
    }
}
